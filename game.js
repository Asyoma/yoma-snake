class Block{

    index=0;
    size = 9;
    x=10;
    y=10;
    color = "#202020";

    draw(){
        ctx.beginPath();
        ctx.rect(this.x, this.y, this.size, this.size);
        ctx.fillStyle = this.color;
        ctx.fill();
        ctx.closePath();
    }
    getX(){
        return this.x;
    }
    setX(x){
        this.x = x;
    }
    getY(){
        return this.y;
    }
    setY(y){
        this.y=y;
    }

    setColor(color){
        this.color = color;
    }

    getIndex(){
        return this.index;
    }

    setIndex(index){
        this.index = index;
    }

}


let canvas = document.getElementById("canvas");
let scoore = document.getElementById("scoore-points"); 
let time_ele = document.getElementById("time"); 
let ctx = canvas.getContext("2d");
let snakeArr = [];
let snakeFood=null;
let snakeMovement = "Ready";
let time =null;
let timeout = null;
let speed_time = 500;
let index=0;
clearCanvas =()=>{
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

drawSnake = () =>{
    //console.log(snakeArr);
    snakeArr.forEach(element => {
        element.draw();
    });
}

feedSnake = (callback)=>{
    let t_block;
    switch(snakeMovement){
        case "ArrowLeft":
            t_block = new Block();
            t_block.setX(snakeArr[0].getX()-10);
            t_block.setY(snakeArr[0].getY());
            snakeArr.unshift(t_block);
            callback();
        break;
        case "Ready":
        case "ArrowRight":
            t_block = new Block();
            t_block.setX(snakeArr[0].getX()+10);
            t_block.setY(snakeArr[0].getY());
            snakeArr.unshift(t_block);
            callback();
        break;
        case "ArrowUp":
            t_block = new Block();
            t_block.setX(snakeArr[0].getX());
            t_block.setY(snakeArr[0].getY()-10);
            snakeArr.unshift(t_block);
            callback();
        break;
        case "ArrowDown":
            t_block = new Block();
            t_block.setX(snakeArr[0].getX());
            t_block.setY(snakeArr[0].getY()+10);
            snakeArr.unshift(t_block);
            callback();
        break;
    }
}

makeTime = ()=>{
    time = moment(time).add(1,"s").format("YYYY-MM-DD HH:mm:ss");
    time_ele.innerHTML=moment(time).format("HH:mm:ss");
    
    timeout = setTimeout(()=>{makeTime();},1000)
    index++;
}
 

window.onload = ()=>{
    time = moment().format("YYYY-MM-DD 00:00:00");
    time_ele.innerHTML=moment(time).format("HH:mm:ss");
    let t_block = new Block();
    t_block.setX(60);
    snakeArr.push(t_block);
    for (i = 1; i <5; i++) {
        feedSnake(()=>{
            if(i == 4){
                snakeFood = new Block();
                snakeFood.setX(110);
                snakeFood.setY(20);
                snakeFood.setColor("red");
                snakeFood.draw();
                drawSnake();
             }
        })
    }
    
}

snakeMovementAction = ()=>{

    if(snakeMovement != "GameOver" && snakeMovement != "Ready"){
        snakeMove(()=>{
            switch(snakeMovement){
                case "ArrowUp":
                case "ArrowDown":
                case "ArrowLeft":
                case "ArrowRight":
                    clearCanvas();
                    drawSnake();
                    snakeFood.draw(); 
                    setTimeout(()=>{
                        snakeMovementAction();
                    },speed_time);
                break;
                default:
                    setTimeout(()=>{
                        snakeMovementAction();
                    },speed_time);
                      
                break;
            }
            
        });
        
    }
    else if (snakeMovement == "Ready"){
        setTimeout(()=>{
            snakeMovementAction();
        },speed_time);
    }
    else{
        alert("Game Over!");
    }
    
}

makeFood = (callback)=>{
    let x,y;
    getRandomInt(0,canvas.width-10,(randomInt)=>{
        x = (Math.round(randomInt/10)*10);
        if(x>canvas.width-10){
            x = canvas.width-10;
        }
        getRandomInt(0,canvas.height-10,(randomInt)=>{
            y = (Math.round(randomInt/10)*10);
            if(y>canvas.height-10){
                y = canvas.height-10;
            }
            snakeFood = new Block();
            snakeFood.setY(y);
            snakeFood.setX(x);
            snakeFood.setColor("red");
            callback();
        });
    });
}



isFoodCathed = (headBlock, callback)=>{
    
    if(headBlock.getX() == snakeFood.getX() && headBlock.getY() == snakeFood.getY()){
        callback(true);
    }
    else{
        callback(false);
    }
}

snakeMove = (callback)=>{
    isFoodCathed(snakeArr[0],(stat)=>{
        if(stat == true){
            makeFood(()=>{
                feedSnake(()=>{
                    let s = Number(scoore.innerHTML);
                    if(index>=120 || s >=100){
                        speed_time-=10;
                    }
                    scoore.innerHTML =  s+10;
                    
                    callback();
                });
            });
            
        }
        else{
            for (var i = snakeArr.length-1; i >= 1 ; i--) {
                snakeArr[i].setX(snakeArr[i-1].getX());
                snakeArr[i].setY(snakeArr[i-1].getY());
                if(i == 1){
                    switch(snakeMovement){
                        case "ArrowUp":
                            if(snakeArr[0].getY() - 10 >=0){
                                snakeArr[0].setY(snakeArr[0].getY() - 10);
                                callback();
                                
                            }
                            else{
                                snakeMovement = "GameOver";
                                callback();
                            }
                               
                        break;
                        case "ArrowDown":
                            if(snakeArr[0].getY() + 10 <canvas.height){
                                snakeArr[0].setY(snakeArr[0].getY() + 10);
                                callback();
                            }
                            else{
                                snakeMovement = "GameOver";
                                callback();
                            }
                            
                        break;
                        case "ArrowLeft":
                            if(snakeArr[0].getX() - 10>=0){
                                snakeArr[0].setX(snakeArr[0].getX() - 10);
                                callback();
                            }else{
                                snakeMovement = "GameOver";
                                callback();
                            }
                            
                        break;
                        case "ArrowRight":
                            if(snakeArr[0].getX() + 10<canvas.width){
                                snakeArr[0].setX(snakeArr[0].getX() + 10);
                                callback();
                            }
                            else{
                                snakeMovement = "GameOver";
                                callback();
                            }
                            
                        break;
                        default:
                            snakeMovement = "GameOver";
                            callback();
                        break;
                    }
                }
            }
        }
    });
    
}



window.onkeyup = (event)=>{
    
    if(snakeMovement != "GameOver" && snakeMovement != "Ready" ){
        if(snakeMovement == "ArrowDown" && event.key == "ArrowUp"){
            snakeMovement = "GameOver";
        }
        else if(snakeMovement == "ArroeUp" && event.key == "ArrowDown"){
            snakeMovement = "GameOver";
        }
        else if(snakeMovement == "ArrowLeft" && event.key == "ArrowRight"){
            snakeMovement = "GameOver";
        }
        else if(snakeMovement == "ArroeRight" && event.key == "ArrowLeft"){
            snakeMovement = "GameOver";
        }
        else{
            snakeMovement = event.key;
        }
        
    }
    else if(snakeMovement == "Ready"){
        if(timeout == null){
            makeTime();
            snakeMovement = event.key;
            snakeMovementAction();
            
        }
        
    }
    else{
        alert("Game Over!")
    }
  
};

getRandomInt = (min, max, callback) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    callback(Math.floor(Math.random() * (max - min + 1)) + min);
}